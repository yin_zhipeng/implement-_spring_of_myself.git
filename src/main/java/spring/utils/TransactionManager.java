package spring.utils;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 事务管理器 手动
 *      事务开启
 *      事务提交
 *      事务回滚
 */
public class TransactionManager {
//    //饿汉式单例
//    private TransactionManager(){}
//    private static TransactionManager instance = new TransactionManager();
//    public static TransactionManager getInstance(){
//        return instance;
//    }

    private ConnectionUtils connectionUtils;
    public void setConnectionUtils(ConnectionUtils connectionUtils){
        this.connectionUtils = connectionUtils;
    }

    //开启手动事务控制
    public void beginTransaction() throws SQLException {
        /**开启事务**/
        //1、关闭自动提交事务
        connectionUtils.getCurrentThreadConn().setAutoCommit(false);
    }
    //提交事务
    public void commit() throws SQLException {
        /**提交事务：上面代码都没问题，那么就可以提交事务了**/
        connectionUtils.getCurrentThreadConn().commit();
    }
    //回滚事务
    public void rollback() throws SQLException {
        /**回滚事务：如果程序执行出错，需要回滚事务**/
        connectionUtils.getCurrentThreadConn().rollback();
        System.out.println("执行出错，回滚事务！！！");
    }
}
