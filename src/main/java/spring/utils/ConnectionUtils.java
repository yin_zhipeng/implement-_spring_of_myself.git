package spring.utils;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionUtils {
//    //饿汉式单例，线程安全的
//    private ConnectionUtils(){}
//
//    private static ConnectionUtils instance = new ConnectionUtils();
//
//    public static ConnectionUtils getInstance(){
//        return instance;
//    }

    //ThreadLocal 线程本地对象，每个线程都有一个自己的，我们将连接保存到这里
    private ThreadLocal<Connection> threadLocal = new ThreadLocal<>();

    /**
     * 从当前线程获取连接
     */
    public Connection getCurrentThreadConn() throws SQLException {
        /**判断当前线程是否已经绑定连接，没绑定，就获取连接绑定到线程，否则直接返回已绑定连接**/
        Connection connection = threadLocal.get();
        //当前线程没有绑定就线程池拿，如果绑定了，直接返回绑定的connection
        //if((connection = threadLocal.get()) == null){
        if(connection == null){
            //从连接池拿
            connection = DruidUtils.getInstance().getConnection();
            //绑定到当前线程
            threadLocal.set(connection);
        }
        return connection;
    }
}
