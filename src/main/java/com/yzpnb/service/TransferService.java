package com.yzpnb.service;

public interface TransferService {
    /**
     * 转账
     * @param fromId 从哪来，要转账的用户id
     * @param toId 到哪去，收钱的用户id
     * @param money 转账金额
     */
    public void transfer(String fromId,String toId,int money) throws Exception;
}
