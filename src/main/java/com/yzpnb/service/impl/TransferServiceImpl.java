package com.yzpnb.service.impl;

import com.yzpnb.dao.AccountDao;
import com.yzpnb.entity.Account;
import com.yzpnb.service.TransferService;

public class TransferServiceImpl implements TransferService {

    private AccountDao accountDao;

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    //转账
    @Override
    public void transfer(String fromId, String toId, int money) throws Exception {
        Account a = accountDao.queryAccountById(fromId);//用户A
        Account b = accountDao.queryAccountById(toId);//用户B
        //a转账5元，money - 5
        a.setMoney(a.getMoney() - money);
        //b收到5元，money + 5
        b.setMoney(b.getMoney() + money);

        accountDao.updateAccountById(a);
        int c = 1/0;
        accountDao.updateAccountById(b);
    }
}
