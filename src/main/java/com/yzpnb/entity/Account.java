package com.yzpnb.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Account {
    private String id;
    private String username;
    private Integer money;
}
