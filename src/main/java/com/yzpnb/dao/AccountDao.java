package com.yzpnb.dao;

import com.yzpnb.entity.Account;

public interface AccountDao {
    //根据用户id查询用户信息
    public Account queryAccountById(String id) throws Exception;
    //根据用户id修改用户信息
    public int updateAccountById(Account account) throws Exception;
}
