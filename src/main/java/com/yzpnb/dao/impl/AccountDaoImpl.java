package com.yzpnb.dao.impl;

import com.yzpnb.dao.AccountDao;
import com.yzpnb.entity.Account;
import spring.utils.ConnectionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class AccountDaoImpl implements AccountDao {

    private ConnectionUtils connectionUtils;
    public void setConnectionUtils(ConnectionUtils connectionUtils){
        this.connectionUtils = connectionUtils;
    }

    //根据用户id查询用户信息
    @Override
    public Account queryAccountById(String id) throws Exception {
        // 获取连接，当前线程绑定，记住不要把这个conn给关了，其它操作还要用呢，已经不是一个操作一个连接了
        Connection conn = connectionUtils.getCurrentThreadConn();

        String sql = "select * from account where id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1,id);
        ResultSet result = ps.executeQuery();

        Account account = new Account();
        while(result.next()){
            account.setId(result.getString("id"));
            account.setUsername(result.getString("username"));
            account.setMoney(result.getInt("money"));
        }
        result.close();
        ps.close();
//        conn.close();

        return account;
    }
    //根据用户id修改用户信息
    @Override
    public int updateAccountById(Account account) throws Exception {
        Connection conn = connectionUtils.getCurrentThreadConn();
        String sql = "update account set money = ? where id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1,account.getMoney());
        ps.setString(2,account.getId());
        int i = ps.executeUpdate();
        ps.close();
        //conn.close()

        return i;
    }

}
