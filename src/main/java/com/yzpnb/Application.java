package com.yzpnb;

import com.yzpnb.service.TransferService;
import spring.factory.BeanFactory;
import spring.factory.ProxyFactory;

public class Application {
    public static void main(String[] args) throws Exception {
        TransferService transferService = (TransferService)BeanFactory.getBean("TransferService");
        //通过IoC容器获取代理工厂
        ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("proxyFactory");
        //获取JDK代理对象
        TransferService jdkProxy = (TransferService) proxyFactory.getJDKProxy(transferService);
        //让代理调用方法
        jdkProxy.transfer("1","2",5);

//        //获取cglib代理对象
//        TransferService cglibProxy = (TransferService) ProxyFactory.getInstance().getCglibProxy(transferService);
//        //让代理调用方法
//        cglibProxy.transfer("1","2",5);
    }
}
