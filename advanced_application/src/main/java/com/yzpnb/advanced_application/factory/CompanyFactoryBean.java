package com.yzpnb.advanced_application.factory;

import com.yzpnb.advanced_application.entity.Company;
import org.springframework.beans.factory.FactoryBean;

public class CompanyFactoryBean implements FactoryBean<Company> {
    private String companyInfo;//公司名称，地址，规模

    public void setCompanyInfo(String companyInfo) {
        this.companyInfo = companyInfo;
    }

    //创建对象，并根据isSingleton方法的返回值（如果是true表示单例），决定是否放入单例池中
    @Override
    public Company getObject() throws Exception {
        //创建复杂对象
        Company company = new Company();
        String[] split = companyInfo.split(",");
        company.setName(split[0]);
        company.setAddress(split[1]);
        company.setScale(split[2]);
        return company;
    }

    //规定生成对象的类型
    @Override
    public Class<?> getObjectType() {
        return Company.class;
    }

    //此对象是单例对象
    @Override
    public boolean isSingleton() {
        return true;
    }
}
