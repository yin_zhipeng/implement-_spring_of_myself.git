package com.yzpnb.advanced_application.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.yzpnb.advanced_application.entity.Company;
import com.yzpnb.advanced_application.factory.CompanyFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

@Configuration//标识此类为注解类
//@ComponentScan({"com.yzpnb.advanced_application","spring"})//自动扫描注解
@ComponentScan({"com.yzpnb.advanced_application","spring.utils"})
@PropertySource("classpath:jdbc.properties")//加载指定外部配置资源
@EnableAspectJAutoProxy
@EnableTransactionManagement//spring声明式事务
public class SpringConfiguration {
    //从外部资源文件中，取到值
    @Value("${jdbc.driver}")
    private String driverClassName;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    //配置bean实例
    @Bean("druidDataSource")
    public DruidDataSource createDataSource(){
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(driverClassName);
        druidDataSource.setUrl(url);
        druidDataSource.setUsername(username);
        druidDataSource.setPassword(password);
        return druidDataSource;
    }

    @Bean("jdbcTemplate")
    public JdbcTemplate jdbcTemplate(DruidDataSource druidDataSource){
        return new JdbcTemplate(druidDataSource);
    }
    @Bean("transactionManager")
    public DataSourceTransactionManager transactionManager(DruidDataSource druidDataSource){
        return new DataSourceTransactionManager(druidDataSource);
    }


    @Bean("company")
    public Company createCompany(){
        CompanyFactoryBean companyFactoryBean = new CompanyFactoryBean();
        companyFactoryBean.setCompanyInfo("鹏氏公司,中关村,9999");
        Company company = null;
        try {
            company = companyFactoryBean.getObject();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return company;
    }
}
