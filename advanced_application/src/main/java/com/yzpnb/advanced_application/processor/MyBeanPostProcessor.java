package com.yzpnb.advanced_application.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * 后置处理器，拦截实例化后的对象（实例化了并且属性注入了）
 */
@Component
public class MyBeanPostProcessor implements BeanPostProcessor {
    public MyBeanPostProcessor() {
        System.out.println("BeanPostProcessor实现类构造函数");
    }

    /**
     * 默认处理所有bean，我们可以用if指定特定的bean
     * @param bean bean实例对象
     * @param beanName bean的id
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if("lazyResult".equalsIgnoreCase(beanName)){
            System.out.println("Bean生命周期第六步，MyBeanPostProcessor before方法拦截特定Bean：lazyResult");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if("lazyResult".equalsIgnoreCase(beanName)){
            System.out.println("Bean生命周期第九步，MyBeanPostProcessor after方法拦截特定Bean：lazyResult");
        }
        return bean;
    }
}
