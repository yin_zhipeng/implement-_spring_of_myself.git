package com.yzpnb.advanced_application.entity;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Data
@ToString
@Component("lazyResult")
public class Result implements BeanNameAware , BeanFactoryAware,  ApplicationContextAware , InitializingBean , DisposableBean {
    private String status;
    private String message;
    private B b;
    public Result() {
        System.out.println("lazyResult....构造函数");
    }

    /**
     * Bean生命周期第3步
     * 注册Bean的唯一id
     * @param name Bean的id
     */
    @Override
    public void setBeanName(String name) {
        System.out.println("Bean生命周期第三步，BeanNameAware的setBeanName方法。。。。注册我成为Bean时注册的id为："+name);
    }

    /**
     * Bean生命周期第四步
     * @param beanFactory Bean工厂
     */
    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("Bean生命周期第四步，BeanFactoryAware的setBeanFactory。。。。管理我的Bean工厂为："+beanFactory);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("Bean生命周期第五步，高级容器接口ApplicationContext为："+applicationContext);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Bean生命周期第七步，InitializingBean的afterPropertiesSet方法");
    }

    @PostConstruct
    public void initMethod(){
        System.out.println("Bean生命周期第八步，init-method");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("Bean销毁，DisposableBean的destroy方法");
    }
    @PreDestroy
    public void destoryMethod(){
        System.out.println("bean销毁：destory-method");
    }
}
