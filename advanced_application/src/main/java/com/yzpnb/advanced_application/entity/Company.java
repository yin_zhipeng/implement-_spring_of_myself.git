package com.yzpnb.advanced_application.entity;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Company {
    private String name;
    private String address;
    private String scale;
}
