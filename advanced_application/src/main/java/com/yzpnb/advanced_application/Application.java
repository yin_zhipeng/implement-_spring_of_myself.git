package com.yzpnb.advanced_application;

import com.yzpnb.advanced_application.config.SpringConfiguration;
import com.yzpnb.advanced_application.service.TransferService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import spring.factory.ProxyFactory;

public class Application {
    public static void main(String[] args) throws Exception {
        //加载配置类，通过配置类
//        ApplicationContext applicationContext =
//                new AnnotationConfigApplicationContext(SpringConfiguration.class);
//        TransferService transferService = null;
//        transferService = (TransferService) applicationContext.getBean("TransferService");
//        //通过IoC容器获取代理工厂
//        ProxyFactory proxyFactory = (ProxyFactory) applicationContext.getBean("proxyFactory");
//        //获取JDK代理对象
//        transferService = (TransferService) proxyFactory.getJDKProxy(transferService);
//        transferService.transfer("1","2",5);

        //测试懒加载
//        ClassPathXmlApplicationContext applicationContext =
//                new ClassPathXmlApplicationContext("classpath:lazy-init.xml");

        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(SpringConfiguration.class);
        System.out.println("=================SpringIoC容器初始化完成=======================");
//        System.out.println("获取bean："+applicationContext.getBean("connectionUtils"));
//        System.out.println("获取最终生成的bean："+applicationContext.getBean("company"));
//        System.out.println("获取工厂bean："+applicationContext.getBean("&company"));
        applicationContext.close();
    }
}
