package com.yzpnb.advanced_application.service;

import org.springframework.stereotype.Service;


public interface TransferService {
    /**
     * 转账
     * @param fromId 从哪来，要转账的用户id
     * @param toId 到哪去，收钱的用户id
     * @param money 转账金额
     */
    public void transfer(String fromId,String toId,int money) throws Exception;
    public void declarativeTransactionTransfer(String fromId,String toId,int money) throws Exception;
}
