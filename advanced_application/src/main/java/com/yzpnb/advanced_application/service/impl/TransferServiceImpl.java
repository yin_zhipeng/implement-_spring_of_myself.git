package com.yzpnb.advanced_application.service.impl;


import com.yzpnb.advanced_application.dao.AccountDao;
import com.yzpnb.advanced_application.dao.impl.AccountDaoImpl;
import com.yzpnb.advanced_application.entity.Account;
import com.yzpnb.advanced_application.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("TransferService")
public class TransferServiceImpl implements TransferService {

    @Autowired
    private AccountDao accountDao;


    //转账
    @Override
    public void transfer(String fromId, String toId, int money) throws Exception {
        Account a = accountDao.queryAccountById(fromId);//用户A
        Account b = accountDao.queryAccountById(toId);//用户B
        //a转账5元，money - 5
        a.setMoney(a.getMoney() - money);
        //b收到5元，money + 5
        b.setMoney(b.getMoney() + money);

        accountDao.updateAccountById(a);
        int c = 1/0;
        accountDao.updateAccountById(b);
    }
    //转账，测试声明式事务
    @Override
    @Transactional//细节均使用默认值
    public void declarativeTransactionTransfer(String fromId, String toId, int money) throws Exception {
        Account a = accountDao.springJdbcQueryAccountById(fromId);//用户A
        Account b = accountDao.springJdbcQueryAccountById(toId);//用户B
        //a转账5元，money - 5
        a.setMoney(a.getMoney() - money);
        //b收到5元，money + 5
        b.setMoney(b.getMoney() + money);

        accountDao.springJdbcQueryUpdateAccountById(a);
        int c = 1/0;
        accountDao.springJdbcQueryUpdateAccountById(b);
    }

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }
}
