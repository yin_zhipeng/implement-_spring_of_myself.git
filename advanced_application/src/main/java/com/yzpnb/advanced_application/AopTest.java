package com.yzpnb.advanced_application;

import com.yzpnb.advanced_application.config.SpringConfiguration;
import com.yzpnb.advanced_application.service.TransferService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.swing.*;

public class AopTest {
    public static void main(String[] args) throws Exception {
//        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");/
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        TransferService transferService = (TransferService)applicationContext.getBean("TransferService");
        transferService.transfer("1","2",5);
    }
}
