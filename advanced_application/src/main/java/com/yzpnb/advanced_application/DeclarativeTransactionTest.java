package com.yzpnb.advanced_application;

import com.yzpnb.advanced_application.config.SpringConfiguration;
import com.yzpnb.advanced_application.service.TransferService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试声明式事务
 */
public class DeclarativeTransactionTest {
    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);
//        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:declarativeTransaction.xml");
        TransferService transferService = (TransferService) applicationContext.getBean("TransferService");
        System.out.println(transferService);
        transferService.declarativeTransactionTransfer("1","2",5);
    }
}
