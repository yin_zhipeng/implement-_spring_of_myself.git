package com.yzpnb.advanced_application.controller;

import com.yzpnb.advanced_application.service.TransferService;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import spring.factory.ProxyFactory;
import spring.utils.ConnectionUtils;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name="transferServlet",urlPatterns = "/transferServlet")
public class TransferServlet extends HttpServlet {
//    //给IoC容器指定xml文件的位置，让它加载
//    private ApplicationContext applicationContext =
//            new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
//    //根据id获取bean
//    private TransferService transferService =
//            (TransferService) applicationContext.getBean("TransferService");
    private TransferService transferService = null;
    @Override
    public void init() throws ServletException {
        WebApplicationContext webApplicationContext =
                WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
        //通过IoC获取TransferService对象
        transferService = (TransferService) webApplicationContext.getBean("TransferService");
        //通过IoC容器获取代理工厂
        ProxyFactory proxyFactory = (ProxyFactory) webApplicationContext.getBean("proxyFactory");
        //获取JDK代理对象，代理TransferService对象
        transferService = (TransferService) proxyFactory.getJDKProxy(transferService);
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //让代理调用方法
        try {
//            transferService.transfer("1","2",5);
            WebApplicationContext webApplicationContext =
                    WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
            ConnectionUtils connectionUtils = (ConnectionUtils) webApplicationContext.getBean("connectionUtils");
            System.out.println(System.identityHashCode(connectionUtils));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


}
