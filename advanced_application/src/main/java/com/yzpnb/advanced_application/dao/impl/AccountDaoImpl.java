package com.yzpnb.advanced_application.dao.impl;

import com.yzpnb.advanced_application.dao.AccountDao;
import com.yzpnb.advanced_application.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import spring.utils.ConnectionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository("accountDao")
public class AccountDaoImpl implements AccountDao {

//    @Autowired
    private ConnectionUtils connectionUtils;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //根据用户id查询用户信息,使用SpringJdbc
    @Override
    public Account springJdbcQueryAccountById(String id) throws Exception {
        String sql = "select * from account where id = ?";
        //queryForObject(sql语句,如何封装结果集,填充上面?的参数)
        return jdbcTemplate.queryForObject(sql, new RowMapper<Account>() {
            @Override
            public Account mapRow(ResultSet resultSet, int i) throws SQLException {
                Account account = new Account();
                account.setId(resultSet.getString("id"));
                account.setUsername(resultSet.getString("username"));
                account.setMoney(resultSet.getInt("money"));
                return account;
            }
        },id);
    }
    @Override
    public int springJdbcQueryUpdateAccountById(Account account) throws Exception {
        String sql = "update account set money = ? where id = ?";
        return jdbcTemplate.update(sql,account.getMoney(),account.getId());
    }
    //根据用户id查询用户信息
    @Override
    public Account queryAccountById(String id) throws Exception {
        // 获取连接，当前线程绑定，记住不要把这个conn给关了，其它操作还要用呢，已经不是一个操作一个连接了
        Connection conn = connectionUtils.getCurrentThreadConn();

        String sql = "select * from account where id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1,id);
        ResultSet result = ps.executeQuery();

        Account account = new Account();
        while(result.next()){
            account.setId(result.getString("id"));
            account.setUsername(result.getString("username"));
            account.setMoney(result.getInt("money"));
        }
        result.close();
        ps.close();
//        conn.close();

        return account;
    }

    //根据用户id修改用户信息
    @Override
    public int updateAccountById(Account account) throws Exception {
        Connection conn = connectionUtils.getCurrentThreadConn();
        String sql = "update account set money = ? where id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1,account.getMoney());
        ps.setString(2,account.getId());
        int i = ps.executeUpdate();
        ps.close();
        //conn.close()

        return i;
    }


    public void setConnectionUtils(ConnectionUtils connectionUtils) {
        this.connectionUtils = connectionUtils;
    }
}
