package com.yzpnb.advanced_application.dao;


import com.yzpnb.advanced_application.entity.Account;
import org.springframework.stereotype.Repository;

public interface AccountDao {
    //根据用户id查询用户信息
    public Account queryAccountById(String id) throws Exception;
    public Account springJdbcQueryAccountById(String id) throws Exception;
    //根据用户id修改用户信息
    public int updateAccountById(Account account) throws Exception;
    public int springJdbcQueryUpdateAccountById(Account account) throws Exception;
}
