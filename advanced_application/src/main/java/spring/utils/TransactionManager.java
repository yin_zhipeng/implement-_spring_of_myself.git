package spring.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

/**
 * 事务管理器 手动
 *      事务开启
 *      事务提交
 *      事务回滚
 */
@Component("transactionManager")
public class TransactionManager {
//    //饿汉式单例
//    private TransactionManager(){}
//    private static TransactionManager instance = new TransactionManager();
//    public static TransactionManager getInstance(){
//        return instance;
//    }

    @Autowired//按类型注入
    @Qualifier("connectionUtils")//如果同类型bean有多个，用这个指定id
    private ConnectionUtils connectionUtils;


    //开启手动事务控制
    public void beginTransaction() throws SQLException {
        /**开启事务**/
        //1、关闭自动提交事务
        connectionUtils.getCurrentThreadConn().setAutoCommit(false);
    }
    //提交事务
    public void commit() throws SQLException {
        /**提交事务：上面代码都没问题，那么就可以提交事务了**/
        connectionUtils.getCurrentThreadConn().commit();
    }
    //回滚事务
    public void rollback() throws SQLException {
        /**回滚事务：如果程序执行出错，需要回滚事务**/
        connectionUtils.getCurrentThreadConn().rollback();
        System.out.println("执行出错，回滚事务！！！");
    }

    public void setConnectionUtils(ConnectionUtils connectionUtils) {
        this.connectionUtils = connectionUtils;
    }
}
