package spring.utils;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@Lazy
@Component("connectionUtils")
public class ConnectionUtils {
    private String[] myArray;
    private Map<String,String> myMap;
    private Set<String> mySet;
    private Properties myProperties;

    public void setMyArray(String[] myArray) {
        this.myArray = myArray;
    }

    public void setMyMap(Map<String, String> myMap) {
        this.myMap = myMap;
    }

    public void setMySet(Set<String> mySet) {
        this.mySet = mySet;
    }

    public void setMyProperties(Properties myProperties) {
        this.myProperties = myProperties;
    }

    //    //饿汉式单例，线程安全的
//    private ConnectionUtils(){}
//
//    private static ConnectionUtils instance = new ConnectionUtils();
//
//    public static ConnectionUtils getInstance(){
//        return instance;
//    }

    //ThreadLocal 线程本地对象，每个线程都有一个自己的，我们将连接保存到这里
    private ThreadLocal<Connection> threadLocal = new ThreadLocal<>();

    public void init(){
        System.out.println("ConnectionUtils====>>>>>初始化方法");
    }
    public void destory(){
        System.out.println("ConnectionUtils====>>>>>销毁方法");
    }

    @Autowired
    private DruidDataSource datasource;

    /**
     * 从当前线程获取连接
     */
    public Connection getCurrentThreadConn() throws SQLException {
        /**判断当前线程是否已经绑定连接，没绑定，就获取连接绑定到线程，否则直接返回已绑定连接**/
        Connection connection = threadLocal.get();
        //当前线程没有绑定就线程池拿，如果绑定了，直接返回绑定的connection
        //if((connection = threadLocal.get()) == null){
        if(connection == null){
            //从连接池拿
            connection = datasource.getConnection();
            //绑定到当前线程
            threadLocal.set(connection);
        }
        return connection;
    }

    public void setDatasource(DruidDataSource datasource) {
        this.datasource = datasource;
    }
}
