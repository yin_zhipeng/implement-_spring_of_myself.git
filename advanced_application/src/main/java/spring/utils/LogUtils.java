package spring.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 日志切面
 */
@Component
@Aspect()
public class LogUtils {
    //切入点
    @Pointcut("execution(* com.yzpnb.advanced_application.service.impl.TransferServiceImpl.*(..))")
    public void tp1(){}

    //业务逻辑执行之前进行横切
    @Before("tp1()")
    public void beforeMethod(){
        System.out.println("业务逻辑执行之前--横切代码.....");
    }
    //业务逻辑执行之后进行横切
    @After("tp1()")
    public void afterMethod(){
        System.out.println("业务逻辑执行完毕后--横切代码.....");
    }
    //业务逻辑抛出异常进行横切
    @AfterThrowing("tp1()")
    public void exceptionMethod(){
        System.out.println("业务逻辑执行错误，抛出异常--横切代码.....");
    }
    //业务逻辑正常运行完成进行横切
    @AfterReturning(value = "tp1()",returning = "retvalue")
    public void successMethod(Object retvalue){
        System.out.println("业务逻辑执行正常结束--横切代码.....");
    }
    //环绕通知
//    @Around("tp1()")
    public void aroundMethod(ProceedingJoinPoint proceedingJoinPoint){
        System.out.println("环绕通知----可以前置通知..........");
        Object result = null;
        try {
            //执行业务逻辑
            result = proceedingJoinPoint.proceed();//相当于动态代理的method.invoke()
        } catch (Throwable throwable) {
            System.out.println("环绕通知----可以异常通知............");
            throw new RuntimeException("环绕通知中，业务逻辑出错");
        }finally {
            System.out.println("环绕通知----可以后置通知...........");
        }
        System.out.println("环绕通知----可以返回通知..........");
    }
}
