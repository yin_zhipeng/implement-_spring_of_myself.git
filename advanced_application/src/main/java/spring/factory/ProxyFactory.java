package spring.factory;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.utils.TransactionManager;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 动态代理工厂
 */
@Component("proxyFactory")
public class ProxyFactory {
//    //单例
//    private ProxyFactory(){}
//    private static ProxyFactory instance = new ProxyFactory();
//    public static ProxyFactory getInstance(){
//        return instance;
//    }

    @Autowired
    private TransactionManager transactionManager;

    //JDK
    public Object getJDKProxy(Object obj){
        //JDK原生，除了对象，还需要接口obj.getClass().getInterfaces()
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), new InvocationHandler() {
            /**
             * 这里编写代理逻辑
             * @param proxy 当前代理对象实例
             * @param method 当前要代理的方法
             * @param args 要代理的方法的参数e
             */
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                /**开启事务**/
                transactionManager.beginTransaction();
                Object result = null;
                try {
                    result = method.invoke(obj, args);
                    /**提交事务：上面代码都没问题，那么就可以提交事务了**/
                    transactionManager.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("JDKProxy--事务回滚！！！");
                    /**回滚事务：如果程序执行出错，需要回滚事务**/
                    transactionManager.rollback();
                }
                return result;
            }
        });
    }

    //CGLIB
    public Object getCglibProxy(Object obj){
        //返回代理对象，和JDK的Proxy类似，MethodInterceptor方法拦截器，可以对各方法进行拦截，和JDK中InvocationHandler效果相同
        return Enhancer.create(obj.getClass(), new MethodInterceptor() {
            /**
             * 这里编写代理逻辑
             * @param o 代理对象实例
             * @param method 当前要代理的方法
             * @param objects 要代理的方法的参数
             * @param methodProxy 当前代理对象代理执行方法的栈顶对象的一个封装
             */
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {

                /**开启事务**/
                transactionManager.beginTransaction();
                Object result = null;
                try {
                    result = method.invoke(obj, objects);
                    /**提交事务：上面代码都没问题，那么就可以提交事务了**/
                    transactionManager.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("CglibProxy--事务回滚！！！");
                    /**回滚事务：如果程序执行出错，需要回滚事务**/
                    transactionManager.rollback();
                }
                return result;
            }
        });
    }

    public void setTransactionManager(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }
}
