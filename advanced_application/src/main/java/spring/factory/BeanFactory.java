package spring.factory;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 工厂类，使用反射生产对象
 */
public class BeanFactory {
    //保存实例化对象，xml中id为key，通过反射创建的实例化对象为value
    private static Map<String,Object> map = new HashMap<>();

    //静态代码块，读取解析xml，实例化对象放map
    static {
        //加载xml
        InputStream is = BeanFactory.class.getClassLoader().getResourceAsStream("applicationContext.xml");
        //解析xml
        SAXReader saxReader = new SAXReader();

        try {
            //拿到xml文档
            Document document = saxReader.read(is);
            //拿到根元素
            Element root = document.getRootElement();
            //搜索当前标签（root根标签<beans>）下所有的<bean>标签
            List<Element> beanList = root.selectNodes("//bean");
            for (int i = 0; i < beanList.size(); i++) {
                Element element =  beanList.get(i);
                //处理每个bean，获取id和class属性
                String id = element.attributeValue("id");//id
                String aClass = element.attributeValue("class");//全限定类名
                //通过反射创建对象
                Class<?> clazz = Class.forName(aClass);//获取类对象
                Object instance = clazz.newInstance();//调用构造方法，获取实例对象
                //存储到map中
                map.put(id,instance);
            }
            //实例化完成后，维护对象的依赖关系，检查哪些对象需要传值，根据配置，传入相应值

            //拿到所有property配置对象
            List<Element> propertyList = root.selectNodes("//property");
            //解析property,获取父元素
            for (int i = 0; i < propertyList.size(); i++) {
                Element element =  propertyList.get(i);//<property name="AccountDao" ref="accountDao"></property>
                String name = element.attributeValue("name");//AccountDao
                String ref = element.attributeValue("ref");//accountDao

                //获取父节点，我们要调用它的set方法
                Element parent = element.getParent();//<bean id = "TransferService" class="com.yzpnb.service.impl.TransferServiceImpl">
                //获取父节点id，从map中找实例
                String parentId = parent.attributeValue("id");//TransferService
                //通过id从map中获取实例对象
                Object parentObject = map.get(parentId);
                //反射对象所有方法，找到"set"+name ===>>> "setAccountDao"方法
                Method[] methods = parentObject.getClass().getMethods();
                for (int j = 0; j < methods.length; j++) {
                    Method method = methods[j];
                    //如果当前方法是相应的set方法
                    if(method.getName().equals("set"+name)){
                        //执行这个方法，并传参，ref就是另一个bean实例的id
                        method.invoke(parentObject,map.get(ref));
                        break;
                    }
                }
                //处理后的实例对象，重新放入map
                map.put(parentId,parentObject);

            }
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    // 静态方法，方便调用，对外提供获取实例对象的接口，根据id获取
    public static Object getBean(String id){
        Object o = map.get(id);
        if(o==null){
            throw new RuntimeException("没有相应实例");
        }
        return o;
    }
}
